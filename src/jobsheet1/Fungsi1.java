/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobsheet1;

/**
 *
 * @author Mamluatul Hani'ah
 */
public class Fungsi1 {

    public static void main(String[] args) {
        int N = 10;

        // Print the first N numbers 
        System.out.println("Fibonanci Rekusif");
        for (int i = 0; i < N; i++) {
            System.out.print(fibRekursi(i) + " ");
        }
        
        System.out.println("\nFibonanci Iteratif");
        FibonacciIteratif(N);
    }

    static void FibonacciIteratif(int N) {
        int num1 = 0, num2 = 1;
        int counter = 0;
        // Iterate till counter is N 
        while (counter < N) {
            // Print the number 
            System.out.print(num1 + " ");
            // Swap 
            int num3 = num2 + num1;
            num1 = num2;
            num2 = num3;
            counter = counter + 1;
        }
    }

    static int fibRekursi(int n) {
        // Base Case 
        if (n <= 1) {
            return n;
        }

        // Recursive call 
        return fibRekursi(n - 1) + fibRekursi(n - 2);
    }
}
